// ==UserScript==
// @name         SmoothKeys
// @description  Scroll smoothly using your keyboard arrow keys. Hold Shift to scroll faster. Should only work when no input fields have focus and nothing in the document has been selected (hit Escape to deselect everything).
// @version      1.0.1
// @namespace    https://andrew.tosk.in/
// @include      *
// @exclude      http*://localhost*
// @exclude      http*://127.0.0.1/*
// @exclude      http*://192.168.*
// @exclude      http*://*curefornightmares.com/*
// @exclude      http*://*tobecontinuedcomic.com/*
// ==/UserScript==

"use strict";


// `keyboard` keeps track of which keys are currently being pressed/released, as
// boolean values.
var keyboard = {
    // Modifier keys. Only Shift is currently used, the others we watch to make
    // sure we don't interfere with default browser or OS commands.
    ALT   : false,
    CTRL  : false,
    SUPER : false,
    SHIFT : false,
    // Keys which actually issue navigation commands.
    ESC   : false,
    DOWN  : false,
    UP    : false,
    LEFT  : false,
    RIGHT : false
};

document.addEventListener( "keydown", function ( event ) {
    if ( event.key === "Alt" || event.key === "AltGraph" || event.keyCode === 18 ) {
        keyboard.ALT = true;
    }
    if ( event.key === "Control" || event.keyCode === 17 ) {
        keyboard.CTRL = true;
    }
    if ( event.key === "Hyper" || event.key === "Meta" || event.key === "Super"
      || event.key === "Hyper" || event.key === "OS" || event.keyCode === 91 || event.keyCode === 92 ) {
        keyboard.SUPER = true;
    }
    if ( event.key === "Shift" || event.keyCode === 16 ) {
        keyboard.SHIFT = true;
    }

    if ( event.key === "Escape" || event.key === "Esc" || event.keyCode === 27 ) {
        keyboard.ESC = true;
    }

    if ( event.key === "ArrowDown" || event.key === "Down" || event.keyCode === 40 ) {
        keyboard.DOWN = true;
    }
    else if ( event.key === "ArrowUp" || event.key === "Up" || event.keyCode === 38 ) {
        keyboard.UP = true;
    }

    if ( event.key === "ArrowRight" || event.key === "Right" || event.keyCode === 39 ) {
        keyboard.RIGHT = true;
    }
    else if ( event.key === "ArrowLeft" || event.key === "Left" || event.keyCode === 37 ) {
        keyboard.LEFT = true;
    }
}, {
    capture: true,
    passive: true
});

document.addEventListener( "keyup", function ( event ) {
    if ( event.key === "Alt" || event.key === "AltGraph" || event.keyCode === 18 ) {
        keyboard.ALT = false;
    }
    if ( event.key === "Control" || event.keyCode === 17 ) {
        keyboard.CTRL = false;
    }
    if ( event.key === "Hyper" || event.key === "Meta" || event.key === "Super"
      || event.key === "Hyper" || event.key === "OS" || event.keyCode === 91 || event.keyCode === 92 ) {
        keyboard.SUPER = false;
    }
    if ( event.key === "Shift" || event.keyCode === 16 ) {
        keyboard.SHIFT = false;
    }

    if ( event.key === "Escape" || event.key === "Esc" || event.keyCode === 27 ) {
        keyboard.ESC = false;
    }

    if ( event.key === "ArrowDown" || event.key === "Down" || event.keyCode === 40 ) {
        keyboard.DOWN = false;
    }
    if ( event.key === "ArrowUp" || event.key === "Up" || event.keyCode === 38 ) {
        keyboard.UP = false;
    }

    if ( event.key === "ArrowRight" || event.key === "Right" || event.keyCode === 39 ) {
        keyboard.RIGHT = false;
    }
    if ( event.key === "ArrowLeft" || event.key === "Left" || event.keyCode === 37 ) {
        keyboard.LEFT = false;
    }
}, {
    capture: true,
    passive: true
});

// Default scrolling speed, in pixels-per-animation-frame.
var speed = 20;
// Boosted scrolling speed.
var superSpeed = 100;

// Loop that watches keyboard inputs and actually does the scrolling and
// navigation.
var scrollNav = function () {
    // Escape always works: Deselect everything and reset all keys.
    if ( keyboard.ESC ) {
        window.getSelection().removeAllRanges();
        document.activeElement.blur();
        keyboard.ALT    = false;
        keyboard.CTRL   = false;
        keyboard.SHIFT  = false;
        keyboard.SUPER  = false;
        keyboard.ESC    = false;
        keyboard.DOWN   = false;
        keyboard.UP     = false;
        keyboard.LEFT   = false;
        keyboard.RIGHT  = false;
    }
    // Skip our custom navigation if anything in the page has focus or selection,
    // or if we're holding any other modifier keys
    if ( document.getSelection().anchorNode !== null || document.activeElement !== document.body
      || keyboard.ALT === true || keyboard.CTRL === true || keyboard.SUPER === true ) {
        window.requestAnimationFrame( scrollNav );
    }
    else {
        // Smoother page scrolling with the arrow keys. Faster when holding Shift key.
        if ( keyboard.SHIFT && keyboard.DOWN ) {
            window.scrollBy( 0, superSpeed );
        }
        else if ( keyboard.SHIFT && keyboard.UP ) {
            window.scrollBy( 0, -superSpeed );
        }
        else if ( keyboard.DOWN ) {
            window.scrollBy( 0, speed );
        }
        else if ( keyboard.UP ) {
            window.scrollBy( 0, -speed );
        }

        if ( keyboard.SHIFT && keyboard.RIGHT ) {
            window.scrollBy( superSpeed, 0 );
        }
        else if ( keyboard.SHIFT && keyboard.LEFT ) {
            window.scrollBy( -superSpeed, 0 );
        }
        else if ( keyboard.RIGHT ) {
            window.scrollBy( speed, 0 );
        }
        else if ( keyboard.LEFT ) {
            window.scrollBy( -speed, 0 );
        }

        window.requestAnimationFrame( scrollNav );
    }
};

window.requestAnimationFrame( scrollNav );
